library(NORMT3)
library(fBasics)
library(dplyr)

# run_simulations is used to run the simulateData function for each noise level, simulating data based on the model and parameters
# this is based on actually sampling from distributions based on parameters
# mode == 1 is the Bayesian model
# mode == 2 is the Rescaling model
# this returns a dataframe subjData with simulated data for all noise conditions and alteration levels

run_simulations = function(fit, ntrials, mode, nRNotMe, bufferFact) {
  if (typeof(fit)=="S4"){
    sigmaLow = unname(coef(fit)[1])
    sigmaHigh = unname(coef(fit)[2])
    crit = unname(coef(fit)[3])
  }else {
    sigmaLow = fit[1]
    sigmaHigh = fit[2]
    crit = fit[3]
  }
    dataLow = simulateData(sigmaLow, crit, ntrials, mode, nRNotMe, bufferFact)
    dataLow$Noise =  0
    dataHigh = simulateData(sigmaHigh, crit, ntrials, mode, nRNotMe, bufferFact)
    dataHigh$Noise = 4
    simData = rbind(dataLow, dataHigh)
  
  return(simData)
}


simulateData = function(noise.sigma, crit, ntrials, mode, nRNotMe, buffer) {
  alts = c(0, 0.07, 0.1, 0.2)
  
  ## Generate stimuli
  nRMe = 6-nRNotMe
  
  noise.mu = 0;
  
  data <- data.frame(d = numeric(ntrials),
                     p = numeric(ntrials))
  
  data$d = c(rep(0, ntrials/4), rep(0.07, ntrials/4), rep(0.1, ntrials/4), rep(0.2, ntrials/4))
  data$p = rnorm(n=ntrials, mean=data$d+noise.mu, sd=noise.sigma)
  
  data$outcome <- (data$d >= crit) == (data$p >= crit) 
  data$response <- Heaviside(data$p, a=crit)
  
  if (mode == 'Bayesian'){
    data$confidence <- 0
    data$cdf = 0.5*(1 + erf(((abs(data$p-crit)+crit)-crit)/(noise.sigma*sqrt(2))))
    data$cdf <- Re(data$cdf)
    data$confidence = data$cdf
    confAcrossAlts = group_by(data, d) %>%
      summarize(meanConf = mean(confidence))
    
    me = dplyr::filter(data, data$response==0)
    notMe = dplyr::filter(data, data$response==1)
    
    binnedConf_NotMe = hist(notMe$confidence, breaks = seq(min(notMe$confidence), max(notMe$confidence), length.out = nRNotMe+1))
    notMe$binnedConf = 0
    notMe$soa = 0
    for (i in 1:nRNotMe){
      notMe$binnedConf[which(between(notMe$confidence, binnedConf_NotMe$breaks[i], binnedConf_NotMe$breaks[i+1]))] = i
      notMe$soa[which(between(notMe$confidence, binnedConf_NotMe$breaks[i], binnedConf_NotMe$breaks[i+1]))] = nRNotMe+1-i
    }
    binnedConf_Me = hist(me$confidence, breaks = seq(min(me$confidence), max(me$confidence), length.out = nRMe+1))
    me$binnedConf = 0
    me$soa = 0
    for (i in 1:nRMe){
      me$binnedConf[which(between(me$confidence, binnedConf_Me$breaks[i], binnedConf_Me$breaks[i+1]))] = i
      me$soa[which(between(me$confidence, binnedConf_Me$breaks[i], binnedConf_Me$breaks[i+1]))] = i+nRNotMe
    }
    
    data = rbind(me, notMe)
  }else if (mode=='Rescaling'){ 
    data$binnedConf = 0
    buffer = buffer*noise.sigma
    a = alts[4] + buffer
    b = alts[1] - buffer
    adist = abs(a-crit)
    bdist = abs(b-crit)
    dist = max(c(adist,bdist))
    notMedist = dist/nRNotMe
    medist = dist/nRMe
    data$confidence = data$p
    data$soa = 0
    
    me = dplyr::filter(data, data$response==0)
    notMe = dplyr::filter(data, data$response==1)
    
    lastBound = crit
    for (i in 1:(nRNotMe-1)){
      notMe$binnedConf[which(between(notMe$confidence, lastBound, lastBound+notMedist))] = i
      notMe$soa[which(between(notMe$confidence, lastBound, lastBound+notMedist))] = nRNotMe+1-i
      lastBound = lastBound+notMedist
    }
    notMe$binnedConf[which(notMe$binnedConf==0)] = nRNotMe
    notMe$soa[which(notMe$soa==0)] = 1
    
    lastBound = crit
    for (i in 1:(nRMe-1)){
      me$binnedConf[which(between(me$confidence, lastBound-medist, lastBound))] = i
      me$soa[which(between(me$confidence, lastBound-medist, lastBound))] = i+nRNotMe
      lastBound = lastBound-medist
    }
    me$binnedConf[which(me$binnedConf==0)] = nRMe
    me$soa[which(me$soa==0)] = 6
    
    data = rbind(me, notMe)
    
  }
  return(data)
}

# simulate_data_dists simulates the distributions of predicted trial counts across ratings per noise level and alteration, for each model
# these distributions are what the models were fit based on
# this is based on the likelihoods computed numerically, not based on sampling
# puts data in long format for density plots

simulate_data_dists = function(bayesfit, altfit, ntrials) {
  bayes.sigmaLow = bayesfit[1]
  bayes.sigmaHigh = bayesfit[2]
  bayes.crit = bayesfit[3]
  bayes.nRNotMe = bayesfit[4]
  bayes.nRMe = 6-bayes.nRNotMe
  alt.sigmaLow = altfit[1]
  alt.sigmaHigh = altfit[2]
  alt.crit = altfit[3]
  alt.buffer = altfit[4]
  alt.nRNotMe = altfit[5]
  alt.nRMe = 6-alt.nRNotMe
  
  # Low Noise
  confBounds_NotMe = bayes.crit
  confBounds_Me = bayes.crit
  confStep_NotMe = 0.5/bayes.nRNotMe
  confStep_Me = 0.5/bayes.nRMe
  nextConf = 0.5
  for (i in 2:bayes.nRNotMe){
    nextConf = nextConf + confStep_NotMe
    x = qnorm(nextConf, bayes.crit, bayes.sigmaLow)
    confBounds_NotMe = c(confBounds_NotMe, bayes.crit + abs(x-bayes.crit))
  }
  confBounds_NotMe = c(confBounds_NotMe, Inf)
  
  nextConf = 0.5
  for (i in 2:bayes.nRMe){
    nextConf = nextConf + confStep_Me
    x = qnorm(nextConf, bayes.crit, bayes.sigmaLow)
    confBounds_Me = c(confBounds_Me, bayes.crit - abs(x-bayes.crit))
  }
  confBounds_Me = c(confBounds_Me, -Inf)
  
  alts = c(0, 0.07, 0.1, 0.2)
  
  # Calculate probabilities of ratings
  prSoA_alt1 = c(0,0,0,0,0,0)
  prSoA_alt2 = c(0,0,0,0,0,0)
  prSoA_alt3 = c(0,0,0,0,0,0)
  prSoA_alt4 = c(0,0,0,0,0,0)
  
  for (i in 1:bayes.nRNotMe){
    prSoA_alt1[i] = (pnorm(confBounds_NotMe[(bayes.nRNotMe+2)-i], alts[1], bayes.sigmaLow) - pnorm(confBounds_NotMe[(bayes.nRNotMe+1)-i], alts[1], bayes.sigmaLow))#/(1-pnorm(crit, alts[1], noise.sigmaLow))
    prSoA_alt2[i] = (pnorm(confBounds_NotMe[(bayes.nRNotMe+2)-i], alts[2], bayes.sigmaLow) - pnorm(confBounds_NotMe[(bayes.nRNotMe+1)-i], alts[2], bayes.sigmaLow))#/(1-pnorm(crit, alts[2], noise.sigmaLow))
    prSoA_alt3[i] = (pnorm(confBounds_NotMe[(bayes.nRNotMe+2)-i], alts[3], bayes.sigmaLow) - pnorm(confBounds_NotMe[(bayes.nRNotMe+1)-i], alts[3], bayes.sigmaLow))#/(1-pnorm(crit, alts[3], noise.sigmaLow))
    prSoA_alt4[i] = (pnorm(confBounds_NotMe[(bayes.nRNotMe+2)-i], alts[4], bayes.sigmaLow) - pnorm(confBounds_NotMe[(bayes.nRNotMe+1)-i], alts[4], bayes.sigmaLow))#/(1-pnorm(crit, alts[4], noise.sigmaLow))
  }
  for (i in (bayes.nRNotMe+1):6) {
    prSoA_alt1[i] = (pnorm(confBounds_Me[i-bayes.nRNotMe], alts[1], bayes.sigmaLow) - pnorm(confBounds_Me[i-(bayes.nRNotMe-1)], alts[1], bayes.sigmaLow))#/pnorm(crit, alts[1], noise.sigmaLow)
    prSoA_alt2[i] = (pnorm(confBounds_Me[i-bayes.nRNotMe], alts[2], bayes.sigmaLow) - pnorm(confBounds_Me[i-(bayes.nRNotMe-1)], alts[2], bayes.sigmaLow))#/pnorm(crit, alts[2], noise.sigmaLow)
    prSoA_alt3[i] = (pnorm(confBounds_Me[i-bayes.nRNotMe], alts[3], bayes.sigmaLow) - pnorm(confBounds_Me[i-(bayes.nRNotMe-1)], alts[3], bayes.sigmaLow))#/pnorm(crit, alts[3], noise.sigmaLow)
    prSoA_alt4[i] = (pnorm(confBounds_Me[i-bayes.nRNotMe], alts[4], bayes.sigmaLow) - pnorm(confBounds_Me[i-(bayes.nRNotMe-1)], alts[4], bayes.sigmaLow))#/pnorm(crit, alts[4], noise.sigmaLow)
  }
  
  prSoA_alt1_BayesLowNoise = prSoA_alt1
  prSoA_alt2_BayesLowNoise = prSoA_alt2
  prSoA_alt3_BayesLowNoise = prSoA_alt3
  prSoA_alt4_BayesLowNoise = prSoA_alt4
  
  # High Noise
  
  confBounds_NotMe_HighNoise = bayes.crit
  confBounds_Me_HighNoise = bayes.crit
  
  nextConf = 0.5
  for (i in 2:bayes.nRNotMe){
    nextConf = nextConf + confStep_NotMe
    x = qnorm(nextConf, bayes.crit, bayes.sigmaHigh)
    confBounds_NotMe_HighNoise = c(confBounds_NotMe_HighNoise, bayes.crit + abs(x-bayes.crit))
  }
  confBounds_NotMe_HighNoise = c(confBounds_NotMe_HighNoise, Inf)
  
  nextConf = 0.5
  for (i in 2:bayes.nRMe){
    nextConf = nextConf + confStep_Me
    x = qnorm(nextConf, bayes.crit, bayes.sigmaHigh)
    confBounds_Me_HighNoise = c(confBounds_Me_HighNoise, bayes.crit - abs(x-bayes.crit))
  }
  confBounds_Me_HighNoise = c(confBounds_Me_HighNoise, -Inf)
  
  # Calculate probabilities of ratings
  prSoA_alt1_HighNoise = c(0,0,0,0,0,0)
  prSoA_alt2_HighNoise = c(0,0,0,0,0,0)
  prSoA_alt3_HighNoise = c(0,0,0,0,0,0)
  prSoA_alt4_HighNoise = c(0,0,0,0,0,0)
  
  for (i in 1:bayes.nRNotMe){
    prSoA_alt1_HighNoise[i] = (pnorm(confBounds_NotMe_HighNoise[(bayes.nRNotMe+2)-i], alts[1], bayes.sigmaHigh) - pnorm(confBounds_NotMe_HighNoise[(bayes.nRNotMe+1)-i], alts[1], bayes.sigmaHigh))#/(1-pnorm(crit, alts[1], noise.sigmaHigh))
    prSoA_alt2_HighNoise[i] = (pnorm(confBounds_NotMe_HighNoise[(bayes.nRNotMe+2)-i], alts[2], bayes.sigmaHigh) - pnorm(confBounds_NotMe_HighNoise[(bayes.nRNotMe+1)-i], alts[2], bayes.sigmaHigh))#/(1-pnorm(crit, alts[2], noise.sigmaHigh))
    prSoA_alt3_HighNoise[i] = (pnorm(confBounds_NotMe_HighNoise[(bayes.nRNotMe+2)-i], alts[3], bayes.sigmaHigh) - pnorm(confBounds_NotMe_HighNoise[(bayes.nRNotMe+1)-i], alts[3], bayes.sigmaHigh))#/(1-pnorm(crit, alts[3], noise.sigmaHigh))
    prSoA_alt4_HighNoise[i] = (pnorm(confBounds_NotMe_HighNoise[(bayes.nRNotMe+2)-i], alts[4], bayes.sigmaHigh) - pnorm(confBounds_NotMe_HighNoise[(bayes.nRNotMe+1)-i], alts[4], bayes.sigmaHigh))#/(1-pnorm(crit, alts[4], noise.sigmaHigh))
  }
  for (i in (bayes.nRNotMe+1):6) {
    prSoA_alt1_HighNoise[i] = (pnorm(confBounds_Me_HighNoise[i-bayes.nRNotMe], alts[1], bayes.sigmaHigh) - pnorm(confBounds_Me_HighNoise[i-(bayes.nRNotMe-1)], alts[1], bayes.sigmaHigh))#/pnorm(crit, alts[1], noise.sigmaHigh)
    prSoA_alt2_HighNoise[i] = (pnorm(confBounds_Me_HighNoise[i-bayes.nRNotMe], alts[2], bayes.sigmaHigh) - pnorm(confBounds_Me_HighNoise[i-(bayes.nRNotMe-1)], alts[2], bayes.sigmaHigh))#/pnorm(crit, alts[2], noise.sigmaHigh)
    prSoA_alt3_HighNoise[i] = (pnorm(confBounds_Me_HighNoise[i-bayes.nRNotMe], alts[3], bayes.sigmaHigh) - pnorm(confBounds_Me_HighNoise[i-(bayes.nRNotMe-1)], alts[3], bayes.sigmaHigh))#/pnorm(crit, alts[3], noise.sigmaHigh)
    prSoA_alt4_HighNoise[i] = (pnorm(confBounds_Me_HighNoise[i-bayes.nRNotMe], alts[4], bayes.sigmaHigh) - pnorm(confBounds_Me_HighNoise[i-(bayes.nRNotMe-1)], alts[4], bayes.sigmaHigh))#/pnorm(crit, alts[4], noise.sigmaHigh)
  }
  
  prSoA_alt1_BayesHighNoise = prSoA_alt1_HighNoise
  prSoA_alt2_BayesHighNoise = prSoA_alt2_HighNoise
  prSoA_alt3_BayesHighNoise = prSoA_alt3_HighNoise
  prSoA_alt4_BayesHighNoise = prSoA_alt4_HighNoise
  
  #Now do alternative model
  confBounds_NotMe = alt.crit
  confBounds_Me = alt.crit
  a = alts[4] + (alt.buffer*alt.sigmaLow)
  b = alts[1] - (alt.buffer*alt.sigmaLow)
  adist = abs(a-alt.crit)
  bdist = abs(b-alt.crit)
  dist = max(c(adist, bdist))
  notMedist = dist/alt.nRNotMe
  medist = dist/alt.nRMe
  
  nextBound = alt.crit
  for (i in 2:alt.nRNotMe){
    nextBound = nextBound + notMedist
    confBounds_NotMe = c(confBounds_NotMe, nextBound)
  }
  confBounds_NotMe = c(confBounds_NotMe, Inf)
  nextBound = alt.crit
  for (i in 2:alt.nRMe){
    nextBound = nextBound - medist
    confBounds_Me = c(confBounds_Me, nextBound)
  }
  confBounds_Me = c(confBounds_Me, -Inf)
  
  # Calculate probabilities of ratings
  prSoA_alt1 = c(0,0,0,0,0,0)
  prSoA_alt2 = c(0,0,0,0,0,0)
  prSoA_alt3 = c(0,0,0,0,0,0)
  prSoA_alt4 = c(0,0,0,0,0,0)
  
  for (i in 1:alt.nRNotMe){
    prSoA_alt1[i] = (pnorm(confBounds_NotMe[(alt.nRNotMe+2)-i], alts[1], alt.sigmaLow) - pnorm(confBounds_NotMe[(alt.nRNotMe+1)-i], alts[1], alt.sigmaLow))#/(1-pnorm(crit, alts[1], noise.sigmaLow))
    prSoA_alt2[i] = (pnorm(confBounds_NotMe[(alt.nRNotMe+2)-i], alts[2], alt.sigmaLow) - pnorm(confBounds_NotMe[(alt.nRNotMe+1)-i], alts[2], alt.sigmaLow))#/(1-pnorm(crit, alts[2], noise.sigmaLow))
    prSoA_alt3[i] = (pnorm(confBounds_NotMe[(alt.nRNotMe+2)-i], alts[3], alt.sigmaLow) - pnorm(confBounds_NotMe[(alt.nRNotMe+1)-i], alts[3], alt.sigmaLow))#/(1-pnorm(crit, alts[3], noise.sigmaLow))
    prSoA_alt4[i] = (pnorm(confBounds_NotMe[(alt.nRNotMe+2)-i], alts[4], alt.sigmaLow) - pnorm(confBounds_NotMe[(alt.nRNotMe+1)-i], alts[4], alt.sigmaLow))#/(1-pnorm(crit, alts[4], noise.sigmaLow))
  }
  for (i in (alt.nRNotMe+1):6) {
    prSoA_alt1[i] = (pnorm(confBounds_Me[i-alt.nRNotMe], alts[1], alt.sigmaLow) - pnorm(confBounds_Me[i-(alt.nRNotMe-1)], alts[1], alt.sigmaLow))#/pnorm(crit, alts[1], noise.sigmaLow)
    prSoA_alt2[i] = (pnorm(confBounds_Me[i-alt.nRNotMe], alts[2], alt.sigmaLow) - pnorm(confBounds_Me[i-(alt.nRNotMe-1)], alts[2], alt.sigmaLow))#/pnorm(crit, alts[2], noise.sigmaLow)
    prSoA_alt3[i] = (pnorm(confBounds_Me[i-alt.nRNotMe], alts[3], alt.sigmaLow) - pnorm(confBounds_Me[i-(alt.nRNotMe-1)], alts[3], alt.sigmaLow))#/pnorm(crit, alts[3], noise.sigmaLow)
    prSoA_alt4[i] = (pnorm(confBounds_Me[i-alt.nRNotMe], alts[4], alt.sigmaLow) - pnorm(confBounds_Me[i-(alt.nRNotMe-1)], alts[4], alt.sigmaLow))#/pnorm(crit, alts[4], noise.sigmaLow)
  }
  prSoA_alt1_AltLowNoise = prSoA_alt1
  prSoA_alt2_AltLowNoise = prSoA_alt2
  prSoA_alt3_AltLowNoise = prSoA_alt3
  prSoA_alt4_AltLowNoise = prSoA_alt4
  
  confBounds_NotMe_HighNoise = alt.crit
  confBounds_Me_HighNoise = alt.crit
  a = alts[4] + (alt.buffer*alt.sigmaHigh)
  b = alts[1] - (alt.buffer*alt.sigmaHigh)
  adist = abs(a-alt.crit)
  bdist = abs(b-alt.crit)
  dist = max(c(adist, bdist))
  notMedist = dist/alt.nRNotMe
  medist = dist/alt.nRMe
  
  nextBound = alt.crit
  for (i in 2:alt.nRNotMe){
    nextBound = nextBound + notMedist
    confBounds_NotMe_HighNoise = c(confBounds_NotMe_HighNoise, nextBound)
  }
  confBounds_NotMe_HighNoise = c(confBounds_NotMe_HighNoise, Inf)
  nextBound = alt.crit
  for (i in 2:alt.nRMe){
    nextBound = nextBound - medist
    confBounds_Me_HighNoise = c(confBounds_Me_HighNoise, nextBound)
  }
  confBounds_Me_HighNoise = c(confBounds_Me_HighNoise, -Inf)
  
  # Calculate probabilities of ratings
  prSoA_alt1_HighNoise = c(0,0,0,0,0,0)
  prSoA_alt2_HighNoise = c(0,0,0,0,0,0)
  prSoA_alt3_HighNoise = c(0,0,0,0,0,0)
  prSoA_alt4_HighNoise = c(0,0,0,0,0,0)
  
  for (i in 1:alt.nRNotMe){
    prSoA_alt1_HighNoise[i] = (pnorm(confBounds_NotMe_HighNoise[(alt.nRNotMe+2)-i], alts[1], alt.sigmaHigh) - pnorm(confBounds_NotMe_HighNoise[(alt.nRNotMe+1)-i], alts[1], alt.sigmaHigh))#/(1-pnorm(crit, alts[1], noise.sigmaHigh))
    prSoA_alt2_HighNoise[i] = (pnorm(confBounds_NotMe_HighNoise[(alt.nRNotMe+2)-i], alts[2], alt.sigmaHigh) - pnorm(confBounds_NotMe_HighNoise[(alt.nRNotMe+1)-i], alts[2], alt.sigmaHigh))#/(1-pnorm(crit, alts[2], noise.sigmaHigh))
    prSoA_alt3_HighNoise[i] = (pnorm(confBounds_NotMe_HighNoise[(alt.nRNotMe+2)-i], alts[3], alt.sigmaHigh) - pnorm(confBounds_NotMe_HighNoise[(alt.nRNotMe+1)-i], alts[3], alt.sigmaHigh))#/(1-pnorm(crit, alts[3], noise.sigmaHigh))
    prSoA_alt4_HighNoise[i] = (pnorm(confBounds_NotMe_HighNoise[(alt.nRNotMe+2)-i], alts[4], alt.sigmaHigh) - pnorm(confBounds_NotMe_HighNoise[(alt.nRNotMe+1)-i], alts[4], alt.sigmaHigh))#/(1-pnorm(crit, alts[4], noise.sigmaHigh))
  }
  for (i in (alt.nRNotMe+1):6) {
    prSoA_alt1_HighNoise[i] = (pnorm(confBounds_Me_HighNoise[i-alt.nRNotMe], alts[1], alt.sigmaHigh) - pnorm(confBounds_Me_HighNoise[i-(alt.nRNotMe-1)], alts[1], alt.sigmaHigh))#/pnorm(crit, alts[1], noise.sigmaHigh)
    prSoA_alt2_HighNoise[i] = (pnorm(confBounds_Me_HighNoise[i-alt.nRNotMe], alts[2], alt.sigmaHigh) - pnorm(confBounds_Me_HighNoise[i-(alt.nRNotMe-1)], alts[2], alt.sigmaHigh))#/pnorm(crit, alts[2], noise.sigmaHigh)
    prSoA_alt3_HighNoise[i] = (pnorm(confBounds_Me_HighNoise[i-alt.nRNotMe], alts[3], alt.sigmaHigh) - pnorm(confBounds_Me_HighNoise[i-(alt.nRNotMe-1)], alts[3], alt.sigmaHigh))#/pnorm(crit, alts[3], noise.sigmaHigh)
    prSoA_alt4_HighNoise[i] = (pnorm(confBounds_Me_HighNoise[i-alt.nRNotMe], alts[4], alt.sigmaHigh) - pnorm(confBounds_Me_HighNoise[i-(alt.nRNotMe-1)], alts[4], alt.sigmaHigh))#/pnorm(crit, alts[4], noise.sigmaHigh)
  }
  prSoA_alt1_AltHighNoise = prSoA_alt1_HighNoise
  prSoA_alt2_AltHighNoise = prSoA_alt2_HighNoise
  prSoA_alt3_AltHighNoise = prSoA_alt3_HighNoise
  prSoA_alt4_AltHighNoise = prSoA_alt4_HighNoise
  
  ntrials_per_NoiseAlt = ntrials/8
  
  soa = c()
  noise = c()
  alt = c()
  prs = c(prSoA_alt1_BayesLowNoise, prSoA_alt2_BayesLowNoise, prSoA_alt3_BayesLowNoise, prSoA_alt4_BayesLowNoise)
  for (i in 1:4){
    ind = ((i-1)*6) + 1
    pr = prs[ind:(ind+5)]
    soaNew = c(rep(1, round(pr[1]*ntrials_per_NoiseAlt)), rep(2, round(pr[2]*ntrials_per_NoiseAlt)),  rep(3, round(pr[3]*ntrials_per_NoiseAlt)),  rep(4, round(pr[4]*ntrials_per_NoiseAlt)),  rep(5, round(pr[5]*ntrials_per_NoiseAlt)), rep(6, round(pr[6]*ntrials_per_NoiseAlt)))
    noise = c(noise, rep("Low", length(soaNew)))
    alt = c(alt, rep(alts[i], length(soaNew)))
    soa = c(soa, soaNew)
  }
  model = rep("Bayesian", length(soa))
  resultsBayes = data.frame(soa, noise, alt, model)
  
  soa = c()
  noise = c()
  alt = c()
  prs = c(prSoA_alt1_BayesHighNoise, prSoA_alt2_BayesHighNoise, prSoA_alt3_BayesHighNoise, prSoA_alt4_BayesHighNoise)
  for (i in 1:4){
    ind = ((i-1)*6) + 1
    pr = prs[ind:(ind+5)]
    soaNew = c(rep(1, round(pr[1]*ntrials_per_NoiseAlt)), rep(2, round(pr[2]*ntrials_per_NoiseAlt)),  rep(3, round(pr[3]*ntrials_per_NoiseAlt)),  rep(4, round(pr[4]*ntrials_per_NoiseAlt)),  rep(5, round(pr[5]*ntrials_per_NoiseAlt)), rep(6, round(pr[6]*ntrials_per_NoiseAlt)))
    noise = c(noise, rep("High", length(soaNew)))
    alt = c(alt, rep(alts[i], length(soaNew)))
    soa = c(soa, soaNew)
  }
  model = rep("Bayesian", length(soa))
  tempResults = data.frame(soa, noise, alt, model)
  resultsBayes = rbind(resultsBayes, tempResults)
  
  soa = c()
  noise = c()
  alt = c()
  prs = c(prSoA_alt1_AltLowNoise, prSoA_alt2_AltLowNoise, prSoA_alt3_AltLowNoise, prSoA_alt4_AltLowNoise)
  for (i in 1:4){
    ind = ((i-1)*6) + 1
    pr = prs[ind:(ind+5)]
    soaNew = c(rep(1, round(pr[1]*ntrials_per_NoiseAlt)), rep(2, round(pr[2]*ntrials_per_NoiseAlt)),  rep(3, round(pr[3]*ntrials_per_NoiseAlt)),  rep(4, round(pr[4]*ntrials_per_NoiseAlt)),  rep(5, round(pr[5]*ntrials_per_NoiseAlt)), rep(6, round(pr[6]*ntrials_per_NoiseAlt)))
    noise = c(noise, rep("Low", length(soaNew)))
    alt = c(alt, rep(alts[i], length(soaNew)))
    soa = c(soa, soaNew)
  }
  model = rep("Rescaling", length(soa))
  resultsRescaling = data.frame(soa, noise, alt, model)
  
  soa = c()
  noise = c()
  alt = c()
  prs = c(prSoA_alt1_AltHighNoise, prSoA_alt2_AltHighNoise, prSoA_alt3_AltHighNoise, prSoA_alt4_AltHighNoise)
  for (i in 1:4){
    ind = ((i-1)*6) + 1
    pr = prs[ind:(ind+5)]
    soaNew = c(rep(1, round(pr[1]*ntrials_per_NoiseAlt)), rep(2, round(pr[2]*ntrials_per_NoiseAlt)),  rep(3, round(pr[3]*ntrials_per_NoiseAlt)),  rep(4, round(pr[4]*ntrials_per_NoiseAlt)),  rep(5, round(pr[5]*ntrials_per_NoiseAlt)), rep(6, round(pr[6]*ntrials_per_NoiseAlt)))
    noise = c(noise, rep("High", length(soaNew)))
    alt = c(alt, rep(alts[i], length(soaNew)))
    soa = c(soa, soaNew)
  }
  model = rep("Rescaling", length(soa))
  tempResults = data.frame(soa, noise, alt, model)
  resultsRescaling = rbind(resultsRescaling, tempResults)
  
  results = rbind(resultsBayes, resultsRescaling)
  
  return(results)
  
}


